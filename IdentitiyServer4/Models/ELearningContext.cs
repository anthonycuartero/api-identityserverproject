﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentitiyServer4.Models
{
    public class ELearningContext : IdentityDbContext
    {
        public ELearningContext()
        {

        }

        public ELearningContext(DbContextOptions<ELearningContext> options)
            : base(options)
        {

        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}
