﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Response
{
    public class PaginationResponse<T>
    {
        public IEnumerable<T> Data { get; set; }

        public int? Page { get; set; }

        public int? Limit { get; set; }

        public int TotalCount { get; set; }

    }
}
