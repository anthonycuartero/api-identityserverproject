﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Response
{
    public class DataResponse
    {
        public object Data { get; set; }
        public bool Success { get; set; }
        public IEnumerable<string> Error { get; set; }
    }
}
