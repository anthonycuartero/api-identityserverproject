﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Response
{
    public class EnrollmentUpdateResponse : Enrollment
    {
        public List<Module> ModulesInProgress { get; set; }
        public List<Module> CompletedModules { get; set; }
    }
}
