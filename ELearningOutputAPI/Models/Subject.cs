﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models
{
    public class Subject
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public List<Course> Courses { get; set; }
        public int IsPublished { get; set; }
        public string Owner { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
