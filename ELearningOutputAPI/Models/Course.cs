﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models
{
    public class Course
    {
        public Guid Id { get; set; }
        public Guid SubjectId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string Author { get; set; }
        public int Duration { get; set; }
        public int IsPublished { get; set; }
        public List<Module> Modules { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
