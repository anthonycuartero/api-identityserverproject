﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models
{
    public class CompletedModules
    {
        public Guid Id { get; set; }
        public Guid ModuleId { get; set; }
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
