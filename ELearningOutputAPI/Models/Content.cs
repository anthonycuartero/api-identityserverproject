﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models
{
    public class Content
    {
        public Guid Id { get; set; }
        public Guid ModuleId { get; set; }
        public string ContentName { get; set; }
        public string Type { get; set; }
        public int IsPublished { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
