﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Request
{
    public class PaginationModuleRequest : PaginationRequest
    {
        public Guid? Course { get; set; }
        public bool HasContents { get; set; }
        public int Duration{ get; set; }
    }
}
