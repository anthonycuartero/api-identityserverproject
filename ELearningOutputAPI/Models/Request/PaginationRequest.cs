﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Request
{
    public class PaginationRequest
    {
        public PaginationRequest() 
        {
            Page = 1;
            Limit = 50;
        }

        public PaginationRequest(int pageNumber, int pageSize) 
        {
            Page = pageNumber;
            Limit = pageSize;
        }

        public int Page { get; set; }

        public int Limit { get; set; }

        public string Sort { get; set; }

        public string SortDirection { get; set; }

        public string Join { get; set; }

        public string Keyword { get; set; }

        public bool Published { get; set; }


    }
}
