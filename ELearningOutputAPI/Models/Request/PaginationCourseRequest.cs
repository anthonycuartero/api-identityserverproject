﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Request
{
    public class PaginationCourseRequest : PaginationRequest
    {
        public Guid? SubjectId { get; set; }

        public bool Sections { get; set; }
    }
}
