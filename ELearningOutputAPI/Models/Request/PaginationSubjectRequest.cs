﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Request
{
    public class PaginationSubjectRequest : PaginationRequest
    {
        public bool Courses { get; set; }
    }
}
