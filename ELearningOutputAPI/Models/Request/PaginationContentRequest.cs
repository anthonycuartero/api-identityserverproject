﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Request
{
    public class PaginationContentRequest : PaginationRequest
    {
        public Guid? Id { get; set; }
        public Guid? Module { get; set; }
    }
}
