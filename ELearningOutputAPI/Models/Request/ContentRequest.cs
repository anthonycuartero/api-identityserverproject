﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Request
{
    public class ContentRequest
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [Required]
        [JsonPropertyName("moduleId")]
        public Guid ModuleId { get; set; }

        [Required]
        [JsonPropertyName("content")]
        public string Content { get; set; }

        [Required]
        [JsonPropertyName("type")]
        public string Type { get; set; }


        [JsonPropertyName("isPublished")]
        public int IsPublished { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("updatedAt")]
        public DateTime UpdatedAt { get; set; }
    }
}
