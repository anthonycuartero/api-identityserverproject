﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.Request
{
    public class PaginationEnrollmentRequest : PaginationRequest
    {
        public Guid? Student { get; set; }
        public List<Guid> Courses { get; set; }
        public bool Completed { get; set; }
    }
}
