﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Models.ContextModel
{
    public class ELearningContext : IdentityDbContext
    {
        public ELearningContext()
        {

        }

        public ELearningContext(DbContextOptions<ELearningContext> options)
            : base(options)
        {

        }



        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Content> Contents { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<CompletedModules> CompletedModules { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Course>().ToTable("Course");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollment");
            modelBuilder.Entity<Content>().ToTable("Content");
            modelBuilder.Entity<Module>().ToTable("Module");
            modelBuilder.Entity<Subject>().ToTable("Subject");
            modelBuilder.Entity<CompletedModules>().ToTable("CompletedModules");
        }
    }
}
