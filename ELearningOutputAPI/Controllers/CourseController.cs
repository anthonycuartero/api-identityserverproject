﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELearningOutputAPI.Hubs;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using ELearningOutputAPI.Repository.Interface;
using ELearningOutputAPI.Routes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;

namespace ELearningOutputAPI.Controllers
{
    [Authorize]
    [ApiController]
    public class CourseController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;

        public CourseController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<ApplicationSettings> appSettings,
            IUnitOfWork unitOfWork,
            IHubContext<CrudHub> hubContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _unitOfWork = unitOfWork;
            _hubContext = new CrudHub(hubContext);
        }


        [HttpPost(ApiRoutes.Courses.InsertCourses)]
        public async Task<IActionResult> InsertCourse(CourseRequest course)
        {
            var user = await GetCurrentUserLogin();

            var subject = await _unitOfWork.SubjectRepository.GetSpecificSubject(course.SubjectId);

            if(subject == null)
            {
                return Error(404, "Subject cannot be found");
            }

            var courseModel = new Course
            {
                Id = Guid.NewGuid(),
                Title = course.Title, 
                Description = course.Description,
                IsPublished = course.IsPublished,
                Author = $"{user.FirstName} {user.LastName}",
                SubjectId = subject.Id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
            };

            var isSuccess = await _unitOfWork.CourseRepository.InsertCourse(courseModel);
            //var isSuccess = await _unitOfWork.SaveAsync();

            _hubContext.InsertData("Course", courseModel);

            return isSuccess ? Ok(courseModel) : null;
        }

        [HttpGet(ApiRoutes.Courses.GetCourses)]
        public async Task<IActionResult> GetCourses([FromQuery] PaginationCourseRequest request)
        {
            var courses = await _unitOfWork.CourseRepository.GetCourses(request);

            return Ok(new PaginationResponse<Course>
            {
                Data = courses,
                Page = request.Page,
                Limit = request.Limit,
                TotalCount = courses.Count(),
            });
        }

        [HttpGet(ApiRoutes.Courses.GetSpecificCourse)]
        public async Task<IActionResult> GetSpecificCourse(Guid id)
        {
            var courses = await _unitOfWork.CourseRepository.GetSpecificCourse(id);

            if (courses == null)
            {
                return Error(404, "Specific course not found");
            }

            return Ok(courses);
        }


        [HttpPatch(ApiRoutes.Courses.UpdateCourse)]
        public async Task<IActionResult> UpdateCourse(CourseRequest course, Guid id)
        {
            var newCourse = await _unitOfWork.CourseRepository.GetSpecificCourse(id);

            if (newCourse == null)
            {
                return Error(404, "Specific course not found");
            }

            newCourse.IsPublished = course.IsPublished;
            newCourse.Title = course.Title;
            newCourse.Description = course.Description;
            newCourse.UpdatedAt = DateTime.UtcNow;

            var courseResult = await _unitOfWork.CourseRepository.UpdateCourse(newCourse);
            //var courseResult = await _unitOfWork.SaveAsync();

            _hubContext.UpdateData("Course", newCourse);

            if (courseResult)
            {
                return Ok(newCourse);
            }

            return BadRequest();
        }


        [HttpDelete(ApiRoutes.Courses.DeleteCourse)]
        public async Task<IActionResult> DeleteCourse(Guid id)
        {
            var deleteResult = await _unitOfWork.CourseRepository.DeleteCourse(id);
            //var deleteResult = await _unitOfWork.SaveAsync();

            _hubContext.RemoveData("Course", id);

            if (deleteResult)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
