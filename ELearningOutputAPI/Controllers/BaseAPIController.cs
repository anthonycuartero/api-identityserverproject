﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELearningOutputAPI.Hubs;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;

namespace ELearningOutputAPI.Controllers
{
    public abstract class BaseAPIController : ControllerBase
    {
        public UserManager<ApplicationUser> _userManager;
        public SignInManager<ApplicationUser> _signInManager;
        public ApplicationSettings _appSettings;
        public CrudHub _hubContext;

        [NonAction]
        public async Task<ApplicationUser> GetCurrentUserLogin()
        {
            var userEmail = User.Claims.First(x => x.Type == "Email").Value;
            var user = await _userManager.FindByNameAsync(userEmail);
            return user;
        }

        [NonAction]
        public async Task<string> GetCurrentRoleLogin()
        {
            var userEmail = User.Claims.First(x => x.Type == "Email").Value;
            var user = await _userManager.FindByNameAsync(userEmail);
            var roles = await _userManager.GetRolesAsync(user);
            return roles.FirstOrDefault();
        }

        [NonAction]
        public BadRequestObjectResult Error(int statusCode, string message)
        {
            return BadRequest(new { statusCode, message });
        }
    }
}
