﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using ELearningOutputAPI.Repository.Interface;
using ELearningOutputAPI.Routes;
using ELearningOutputAPI.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace ELearningOutputAPI.Controllers
{
    [ApiController]
    public class AccountsController : BaseAPIController
    {
        private readonly IAccountsService _accountsService;
        private readonly IEmailService _emailService;


        public AccountsController(IAccountsService accountsService,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            IEmailService emailService)
        {
            _accountsService = accountsService;
            _signInManager = signInManager;
            _userManager = userManager;
            _emailService = emailService;
        }

        [HttpPost(ApiRoutes.Accounts.Signup)]
        public async Task<IActionResult> Signup([FromBody] ApplicationUserResponse userModel)
        {
            var authResponse = await _accountsService.SignupAsync(userModel);

            if (authResponse.Success)
            {
                return Ok(authResponse.Data);
            }

            return BadRequest(authResponse.Error);
        }

        [HttpPost(ApiRoutes.Accounts.Login)]
        public async Task<IActionResult> Login(LoginRequest login)
        {
            var loginResponse = await _accountsService.LoginAsync(login);

            if (loginResponse.Success)
            {
                return Ok(loginResponse.Data);
            }

            return BadRequest(loginResponse.Error);
        }

        [HttpPost(ApiRoutes.Accounts.Logout)]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }

        [HttpPost(ApiRoutes.Accounts.ResetPassword)]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequest password)
        {
            var user = await GetCurrentUserLogin();

            if (user != null)
            {
                var result = _accountsService.ResetPassword(user, password.Password);
                return Ok(result);
            }

            return Error(404, "User cannot be found");
        }

        [HttpGet(ApiRoutes.Accounts.RequestResetPassword)]
        public async Task<IActionResult> RequestResetPassword([FromQuery] string email)
        {
            var user = _accountsService.GetUserDetailsByEmail(email);

            if (user != null)
            {
                var result = await _emailService.SendEmail(new EmailParams
                {
                    ToEmail = email,
                    Subject = "Request a reset password",
                    Body = "To reset your password please click this link and provide new password https://localhost:44332/password"
                });

                if (result)
                {
                    return Ok(result);
                }
            }

            return Error(404, "User cannot be found");
        }

        [HttpGet(ApiRoutes.Accounts.ResendVerificationEmail)]
        public async Task<IActionResult> ResendVerificationEmail([FromQuery] string email)
        {
            var user = _accountsService.GetUserDetailsByEmail(email);

            if (user != null)
            {
                var result = await _emailService.SendEmail(new EmailParams
                {
                    ToEmail = email,
                    Subject = "Resend verification email",
                    Body = "Please click this link to verify user https://localhost:44332/signup/verification"
                });

                if (result)
                {
                    return Ok(result);
                }
            }

            return Error(404, "User cannot be found");
        }

        [HttpPost(ApiRoutes.Accounts.ExternalLogin)]
        public async Task<IActionResult> ExternalLogin(ExternalLoginModel loginModel)
        {
            var response = await _accountsService.ExternalLogin(loginModel);

            var responseString = await response.Content.ReadAsStringAsync();

            if (((int)response.StatusCode) == 200)
            {
                return Ok(responseString);
            }

            return Error(((int)response.StatusCode), responseString);

        }
    }
}
