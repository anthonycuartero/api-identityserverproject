﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using ELearningOutputAPI.Repository.Interface;
using ELearningOutputAPI.Routes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace ELearningOutputAPI.Controllers
{
    [Authorize]
    [ApiController]
    public class EnrollmentController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;

        public EnrollmentController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<ApplicationSettings> appSettings,
            IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _unitOfWork = unitOfWork;
        }

        [HttpPost(ApiRoutes.Enrollments.InsertEnrollment)]
        public async Task<IActionResult> InsertEnrollment(EnrollmentRequest enrollment)
        {
            var course = await _unitOfWork.CourseRepository.GetSpecificCourse(enrollment.CourseId);

            var user = await GetCurrentUserLogin();

            if (course == null)
            {
                return Error(404, "Course cannot be found");
            }

            var existingEnrollment = await _unitOfWork.EnrollmentsRepository.GetExistingEnrollment(enrollment.CourseId, Guid.Parse(user.Id));

            if (existingEnrollment != null)
            {
                return Error(400, "User already enrolled on this course.");
            }

            var enrollmentModel = new Enrollment
            {
                Id = Guid.NewGuid(),
                UserId = Guid.Parse(user.Id),
                CourseId = course.Id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
            };

            var isSuccess = await _unitOfWork.EnrollmentsRepository.InsertEnrollment(enrollmentModel);
            //var isSuccess = await _unitOfWork.SaveAsync();

            return isSuccess ? Ok(enrollmentModel) : null;
        }

        [HttpGet(ApiRoutes.Enrollments.GetEnrollments)]
        public async Task<IActionResult> GetEnrollments([FromQuery] PaginationEnrollmentRequest request)
        {
            var user = await GetCurrentUserLogin();

            request.Student = Guid.Parse(user.Id);

            var enrollments = await _unitOfWork.EnrollmentsRepository.GetEnrollments(request);

            var courses = await _unitOfWork.CourseRepository.GetCoursesByIds(enrollments.Select(x=>x.CourseId).ToList());

            return Ok(new PaginationResponse<Course>
            {
                Data = courses,
                Page = request.Page,
                Limit = request.Limit,
                TotalCount = courses.Count(),
            });
        }

        [HttpGet(ApiRoutes.Enrollments.GetSpecificEnrollment)]
        public async Task<IActionResult> GetSpecificEnrollment(Guid id)
        {
            var enrollment = await _unitOfWork.EnrollmentsRepository.GetSpecificEnrollment(id);

            if (enrollment == null)
            {
                return Error(404, "Specific enrollment not found");
            }

            var userRole = await GetCurrentRoleLogin();

            var currentUser = await GetCurrentUserLogin();

            if (userRole.Equals("Student"))
            {
                if (enrollment.UserId.ToString() == currentUser.Id)
                {
                    return Ok(enrollment);
                }
                else
                {
                    return Error(401, "You are not authorize to view this enrollment");
                }
            }

            return Ok(enrollment);
        }

        [HttpPatch(ApiRoutes.Enrollments.UpdateEnrollment)]
        public async Task<IActionResult> UpdateEnrollment(EnrollmentRequest enrollment, Guid id)
        {
            var module = await _unitOfWork.ModuleRepository.GetSpecificModule(enrollment.ModuleId);

            var user = await GetCurrentUserLogin();

            if (module == null)
            {
                return Error(404, "Specific module not found");
            }

            var completedModules = new CompletedModules
            {
                Id = Guid.NewGuid(),
                ModuleId = enrollment.ModuleId,
                UserId = Guid.Parse(user.Id),
                CourseId = module.CourseId,
                CreatedAt = DateTime.UtcNow,
            };

            await _unitOfWork.EnrollmentsRepository.InsertCompletedModules(completedModules);

            //Get specific enrollment and update details.
            var newEnrollment = await _unitOfWork.EnrollmentsRepository.GetSpecificEnrollment(id);
            newEnrollment.IsCompleted = enrollment.IsCompleted;
            var moduleResult = await _unitOfWork.EnrollmentsRepository.UpdateEnrollment(newEnrollment);
            //var moduleResult = await _unitOfWork.SaveAsync();

            //Get all ids of completed modules.
            var completedModulesList = await _unitOfWork.EnrollmentsRepository.GetCompletedModulesByUserAndCourse(completedModules.UserId, module.CourseId);
            var completedIds = completedModulesList.Select(x => x.ModuleId).ToArray();

            var modulesByCourse = await _unitOfWork.ModuleRepository.GetModulesByCourseId(module.CourseId);

            var responseResult = new EnrollmentUpdateResponse
            {
                Id = newEnrollment.Id,
                UserId = newEnrollment.UserId,
                CourseId = newEnrollment.CourseId,
                ModulesInProgress = modulesByCourse.Where(x => !completedIds.Contains(x.Id)).ToList(),
                CompletedModules = modulesByCourse.Where(x => completedIds.Contains(x.Id)).ToList(),
                CreatedAt = newEnrollment.CreatedAt,
                UpdatedAt = newEnrollment.UpdatedAt,
            };

            if (moduleResult)
            {
                return Ok(responseResult);
            }

            return BadRequest();
        }

        [HttpDelete(ApiRoutes.Enrollments.DeleteEnrollment)]
        public async Task<IActionResult> DeleteEnrollment(Guid id)
        {
            var deleteResult = await _unitOfWork.EnrollmentsRepository.DeleteEnrollment(id);
            //var deleteResult = await _unitOfWork.SaveAsync();

            if (deleteResult)
            {
                return Ok(deleteResult);
            }

            return BadRequest();
        }
    }
}
