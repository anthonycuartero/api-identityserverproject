﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace ELearningOutputAPI.Controllers
{
    [ApiController]
    [Authorize]
    public class UserController : BaseAPIController
    {
        public UserController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<ApplicationSettings> appSettings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        [Route("users")]
        public async Task<object> GetUsers()
        {
            var users = await _userManager.Users.ToListAsync();

            var userList = users.Select(detail => new ApplicationUserResponse
            {
                Id = detail.Id,
                Email = detail.Email,
                Role = "",
                FirstName = detail.FirstName,
                LastName = detail.LastName,
            }).ToList();

            return Ok(new
            {
                data = userList,
                totalCount = 1,
                page = 1,
                limit = 5,
            });
        }

        [HttpGet]
        [Route("users/{id}")]
        public async Task<object> GetUserDetails(string id)
        {
            var currentRole = await GetCurrentRoleLogin();

            var currentUser = await GetCurrentUserLogin();

            var user = await _userManager.FindByIdAsync(id);

            if(user == null)
            {
                return Error(404, "User cannot be found");
            }

            var role = await _userManager.GetRolesAsync(user);

            if (IsUnauthorize(role.FirstOrDefault(), currentRole, currentUser.Id, id))
            {
                return Error(401, "Student cannot view other student details");
            }

            var userModel = new ApplicationUserResponse
            {
                Id = user.Id,
                Email = user.Email,
                Role = role.FirstOrDefault(),
                FirstName = user.FirstName,
                LastName = user.LastName,
            };

            return Ok(userModel);
        }

        [HttpGet]
        [Route("user")]
        public async Task<object> GetUserDetailsByToken()
        {
            var currentRole = await GetCurrentRoleLogin();

            var currentUser = await GetCurrentUserLogin();

            var userModel = new ApplicationUserResponse
            {
                Id = currentUser.Id,
                Email = currentUser.Email,
                Role = currentRole,
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName,
            };

            return Ok(userModel);
        }

        private static bool IsUnauthorize(string role, string currentRole, string currentUserId, string id)
        {
            return role.FirstOrDefault().Equals("Student") && currentRole.Equals("Student") && currentUserId != id;
        }
    }
}
