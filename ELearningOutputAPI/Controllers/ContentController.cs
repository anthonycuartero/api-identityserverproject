﻿using ELearningOutputAPI.Hubs;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using ELearningOutputAPI.Repository.Interface;
using ELearningOutputAPI.Routes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Controllers
{
    [Authorize]
    [ApiController]
    public class ContentController : BaseAPIController
    {

        private readonly IUnitOfWork _unitOfWork;
        public ContentController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<ApplicationSettings> appSettings,
            IUnitOfWork unitOfWork,
            IHubContext<CrudHub> hubContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _unitOfWork = unitOfWork;
            _hubContext = new CrudHub(hubContext);
        }

        [HttpPost(ApiRoutes.Contents.InsertContent)]
        public async Task<IActionResult> InsertModule(ContentRequest content)
        {
            var module = await _unitOfWork.ModuleRepository.GetSpecificModule(content.ModuleId);

            if (module == null)
            {
                return NotFound(new { message = "Module cannot be found" });
            }

            var contentModel = new Content
            {
                Id = Guid.NewGuid(),
                ContentName = content.Content,
                Type = content.Type,
                IsPublished = content.IsPublished,
                ModuleId = module.Id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
            };

            var isSuccess = await _unitOfWork.ContentRepository.InsertContent(contentModel);
            //var isSuccess = await _unitOfWork.SaveAsync();

            _hubContext.InsertData("Content", contentModel);

            return isSuccess ? Ok(contentModel) : null;
        }

        [HttpGet(ApiRoutes.Contents.GetContents)]
        public async Task<IActionResult> GetContents([FromQuery] PaginationContentRequest request)
        {
            var content = await _unitOfWork.ContentRepository.GetContents(request);

            return Ok(new PaginationResponse<Content>
            {
                Data = content,
                Page = request.Page,
                Limit = request.Limit,
                TotalCount = content.Count(),
            });
        }

        [HttpGet(ApiRoutes.Contents.GetSpecificContent)]
        public async Task<IActionResult> GetSpecificContent(Guid id)
        {
            var content = await _unitOfWork.ContentRepository.GetSpecificContent(id);

            if (content == null)
            {
                return NotFound(new { message = "Specific content not found" });
            }

            return Ok(content);
        }


        [HttpPatch(ApiRoutes.Contents.UpdateContent)]
        public async Task<IActionResult> UpdateContent(ContentRequest content, Guid id)
        {
            var newContent = await _unitOfWork.ContentRepository.GetSpecificContent(id);

            if (newContent == null)
            {
                return NotFound(new { message = "Specific content not found" });
            }

            newContent.IsPublished = content.IsPublished;
            newContent.ContentName = content.Content;
            newContent.Type = content.Type;
            newContent.UpdatedAt = DateTime.UtcNow;

            var moduleResult = await _unitOfWork.ContentRepository.UpdateContent(newContent);
            //var moduleResult = await _unitOfWork.SaveAsync();

            _hubContext.UpdateData("Content", newContent);

            if (moduleResult)
            {
                return Ok(newContent);
            }

            return BadRequest();
        }


        [HttpDelete(ApiRoutes.Contents.DeleteContent)]
        public async Task<IActionResult> DeleteContent(Guid id)
        {
            var contentResult = await _unitOfWork.ContentRepository.DeleteContent(id);
            //var contentResult = await _unitOfWork.SaveAsync();

            _hubContext.RemoveData("Content", id);

            if (contentResult)
            {
                return Ok(contentResult);
            }

            return BadRequest();
        }
    }
}
