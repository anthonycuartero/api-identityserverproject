﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELearningOutputAPI.Hubs;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using ELearningOutputAPI.Repository.Interface;
using ELearningOutputAPI.Routes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;

namespace ELearningOutputAPI.Controllers
{
    [Authorize]
    [ApiController]
    public class SubjectController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;
        public SubjectController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<ApplicationSettings> appSettings,
            IUnitOfWork unitOfWork,
            IHubContext<CrudHub> hubContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _unitOfWork = unitOfWork;
            _hubContext = new CrudHub(hubContext);
        }


        [HttpPost(ApiRoutes.Subjects.InsertSubjects)]
        public async Task<IActionResult> InsertSubject(SubjectRequest subject)
        {
            try
            {
                var user = await GetCurrentUserLogin();

                var subjectModel = new Subject
                {
                    Id = Guid.NewGuid(),
                    Title = subject.Title,
                    IsPublished = 0,
                    Owner = $"{user.FirstName} {user.LastName}",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                };

                var isSuccess = await _unitOfWork.SubjectRepository.InsertSubject(subjectModel);
                //var isSuccess = await _unitOfWork.SaveAsync();

                _hubContext.InsertData("Subject", subjectModel);


                return isSuccess ? Ok(subjectModel) : null;
            }
            catch(Exception ex)
            {
                return Error(500, ex.Message);
            }

            
        }

        [HttpGet(ApiRoutes.Subjects.GetSubjects)]
        public async Task<IActionResult> GetSubjects([FromQuery] PaginationSubjectRequest request)
        {
            var subjects = await _unitOfWork.SubjectRepository.GetSubjects(request);

            return Ok(new PaginationResponse<Subject>
            {
                Data = subjects,
                Page = request.Page,
                Limit = request.Limit,
                TotalCount = subjects.Count(),
            });
        }


        [HttpGet(ApiRoutes.Subjects.GetSpecificSubject)]
        public async Task<IActionResult> GetSpecificSubject(Guid subjectId)
        {
            var subjects = await _unitOfWork.SubjectRepository.GetSpecificSubject(subjectId);

            if(subjects != null)
            {
                return Ok(subjects);
            }

            return BadRequest(subjects);
        }


        [HttpPatch(ApiRoutes.Subjects.UpdateSubject)]
        public async Task<IActionResult> UpdateSubject(SubjectRequest subject, Guid id)
        {
            var newSubject = await _unitOfWork.SubjectRepository.GetSpecificSubject(id);

            if(newSubject == null)
            {
                return Error(404, "Specific subject not found");
            }

            newSubject.IsPublished = subject.IsPublished;
            newSubject.Title = subject.Title;
            newSubject.UpdatedAt = DateTime.UtcNow;

            var subjectResult = await _unitOfWork.SubjectRepository.UpdateSubject(newSubject);
            //var subjectResult = await _unitOfWork.SaveAsync();

            _hubContext.UpdateData("Subject", newSubject);

            if (subjectResult)
            {
                return Ok(newSubject);
            }

            return BadRequest();
        }


        [HttpDelete(ApiRoutes.Subjects.DeleteSubject)]
        public async Task<IActionResult> DeleteSubject(Guid id)
        {
            var deleteResult = await _unitOfWork.SubjectRepository.DeleteSubject(id);
            //var deleteResult = await _unitOfWork.SaveAsync();

            _hubContext.RemoveData("Subject", id);

            if (deleteResult)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
