﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELearningOutputAPI.Hubs;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using ELearningOutputAPI.Repository.Interface;
using ELearningOutputAPI.Routes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;

namespace ELearningOutputAPI.Controllers
{
    [Authorize]
    [ApiController]
    public class ModuleController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;
        public ModuleController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<ApplicationSettings> appSettings,
            IUnitOfWork unitOfWork,
            IHubContext<CrudHub> hubContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _unitOfWork = unitOfWork;
            _hubContext = new CrudHub(hubContext);
        }

        [HttpPost(ApiRoutes.Modules.InsertModule)]
        public async Task<IActionResult> InsertModule(ModuleRequest module)
        {
            var course = await _unitOfWork.CourseRepository.GetSpecificCourse(module.CourseId);

            if (course == null)
            {
                return Error(404, "Course cannot be found");
            }

            var moduleModel = new Module
            {
                Id = Guid.NewGuid(),
                Title = module.Title,
                Duration = module.Duration,
                IsPublished = module.IsPublished,
                CourseTitle = course.Title,
                CourseId = course.Id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
            };

            var isSuccess = await _unitOfWork.ModuleRepository.InsertModule(moduleModel);
            //var isSuccess = await _unitOfWork.SaveAsync();

            _hubContext.InsertData("Module", moduleModel);

            return isSuccess ? Ok(moduleModel) : null;
        }

        [HttpGet(ApiRoutes.Modules.GetModules)]
        public async Task<IActionResult> GetModules([FromQuery] PaginationModuleRequest request)
        {
            var modules = await _unitOfWork.ModuleRepository.GetModules(request);

            return Ok(new PaginationResponse<Module>
            {
                Data = modules,
                Page = request.Page,
                Limit = request.Limit,
                TotalCount = modules.Count(),
            });
        }

        [HttpGet(ApiRoutes.Modules.GetSpecificModule)]
        public async Task<IActionResult> GetSpecificModule(Guid id)
        {
            var courses = await _unitOfWork.ModuleRepository.GetSpecificModule(id);

            if (courses == null)
            {
                return Error(404, "Specific module not found");
            }

            return Ok(courses);
        }

        [HttpPatch(ApiRoutes.Modules.UpdateModule)]
        public async Task<IActionResult> UpdateModule(ModuleRequest module, Guid id)
        {
            var newModule = await _unitOfWork.ModuleRepository.GetSpecificModule(id);

            if (newModule == null)
            {
                return Error(404, "Specific module not found");
            }

            newModule.IsPublished = module.IsPublished;
            newModule.Title = module.Title;
            newModule.Duration = module.Duration;
            newModule.UpdatedAt = DateTime.UtcNow;

            var moduleResult = await _unitOfWork.ModuleRepository.UpdateModule(newModule);
            //var moduleResult = await _unitOfWork.SaveAsync();

            _hubContext.UpdateData("Module", newModule);

            if (moduleResult)
            {
                return Ok(newModule);
            }

            return BadRequest();
        }

        [HttpDelete(ApiRoutes.Modules.DeleteModule)]
        public async Task<IActionResult> DeleteModule(Guid id)
        {
            var deleteResult = await _unitOfWork.ModuleRepository.DeleteModule(id);
            //var deleteResult = await _unitOfWork.SaveAsync();

            _hubContext.RemoveData("Module", id);

            if (deleteResult)
            {
                return Ok(deleteResult);
            }

            return BadRequest();
        }
    }
}
