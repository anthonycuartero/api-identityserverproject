﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Hubs
{
    public interface ICrudHub
    {
        void InsertData(string groupName, object data);
        void RemoveData(string groupName, object data);
        void UpdateData(string groupName, object data);
        void AddToGroup(string groupName);
    }
}
