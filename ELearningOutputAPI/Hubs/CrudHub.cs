﻿using Microsoft.AspNetCore.SignalR;

namespace ELearningOutputAPI.Hubs
{
    public class CrudHub : Hub
    {
        private readonly IHubContext<CrudHub> _context;
        public CrudHub(IHubContext<CrudHub> context)
        {
            _context = context;
        }

        public void InsertData(string groupName, object data)
        {
            _context.Clients.Group(groupName).SendAsync("InsertData", data);
        }

        public void RemoveData(string groupName, object data)
        {
            _context.Clients.Group(groupName).SendAsync("RemoveData", data);
        }

        public void UpdateData(string groupName, object data)
        {
            _context.Clients.Group(groupName).SendAsync("UpdateData", data);
        }

        public void AddToGroup(string groupName)
        {
            _context.Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            //await Clients.Group(groupName).SendAsync("OnlineNotification", $"{userName} is now online");
        }
    }
}
