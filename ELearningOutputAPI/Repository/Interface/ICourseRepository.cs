﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Interface
{
    public interface ICourseRepository
    {
        Task<List<Course>> GetCourses(PaginationCourseRequest request);
        Task<List<Course>> GetCoursesByIds(List<Guid> courseIds);
        Task<Course> GetSpecificCourse(Guid courseId);
        Task<bool> InsertCourse(Course course);
        Task<bool> UpdateCourse(Course course);
        Task<bool> DeleteCourse(Guid courseId);
    }
}
