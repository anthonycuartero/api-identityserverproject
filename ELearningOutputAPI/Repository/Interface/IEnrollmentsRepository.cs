﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Interface
{
    public interface IEnrollmentsRepository
    {
        Task<List<Enrollment>> GetEnrollments(PaginationEnrollmentRequest request);
        Task<Enrollment> GetSpecificEnrollment(Guid enrollmentId);
        Task<Enrollment> GetExistingEnrollment(Guid courseId, Guid studentId);
        Task<bool> InsertEnrollment(Enrollment enrollment);
        Task<bool> UpdateEnrollment(Enrollment enrollment);
        Task<bool> DeleteEnrollment(Guid enrollmentId);

        Task<bool> InsertCompletedModules(CompletedModules enrollment);
        Task<List<CompletedModules>> GetCompletedModulesByUserAndCourse(Guid userId, Guid courseId);
    }
}
