﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Interface
{
    public interface IModulesRepository
    {
        Task<List<Module>> GetModules(PaginationModuleRequest request);
        Task<Module> GetSpecificModule(Guid moduleId);
        Task<bool> InsertModule(Module module);
        Task<bool> UpdateModule(Module module);
        Task<bool> DeleteModule(Guid moduleId);

        Task<List<Module>> GetModulesByCourseId(Guid courseId);
    }
}
