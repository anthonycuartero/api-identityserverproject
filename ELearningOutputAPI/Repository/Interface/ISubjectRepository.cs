﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Interface
{
    public interface ISubjectRepository
    {
        Task<List<Subject>> GetSubjects(PaginationSubjectRequest request);
        Task<Subject> GetSpecificSubject(Guid subjectId);
        Task<bool> InsertSubject(Subject subject);
        Task<bool> UpdateSubject(Subject subject);
        Task<bool> DeleteSubject(Guid subjectId);
    }
}
