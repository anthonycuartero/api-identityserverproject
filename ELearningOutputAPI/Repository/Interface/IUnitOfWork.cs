﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Interface
{
    public interface IUnitOfWork
    {
        ISubjectRepository SubjectRepository { get; }
        ICourseRepository CourseRepository { get; }
        IModulesRepository ModuleRepository { get; }
        IContentRepository ContentRepository { get; }
        IEnrollmentsRepository EnrollmentsRepository { get; }

        Task<bool> SaveAsync();
    }
}
