﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Interface
{
    public interface IContentRepository
    {
        Task<List<Content>> GetContents(PaginationContentRequest request);
        Task<Content> GetSpecificContent(Guid contentId);
        Task<bool> InsertContent(Content content);
        Task<bool> UpdateContent(Content content);
        Task<bool> DeleteContent(Guid content);
    }
}
