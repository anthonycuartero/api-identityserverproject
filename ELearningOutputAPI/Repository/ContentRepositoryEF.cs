﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository
{
    public class ContentRepositoryEF : IContentRepository
    {
        private ELearningContext _dbContext;

        public ContentRepositoryEF(ELearningContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> DeleteContent(Guid contentId)
        {
            if (_dbContext != null)
            {
                var content = await GetSpecificContent(contentId);
                _dbContext.Contents.Remove(content);
                return true;
            }

            return false;
        }

        public async Task<Content> GetSpecificContent(Guid contentId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Contents.FirstOrDefaultAsync(x => x.Id == contentId);
            }

            return null;
        }

        public async Task<List<Content>> GetContents(PaginationContentRequest request)
        {
            if (_dbContext != null)
            {
                if (request == null)
                {
                    return await _dbContext.Contents.ToListAsync();
                }

                var contents = _dbContext.Contents.AsQueryable();

                contents = FilterContents(request, contents);

                contents = SortContents(request, contents);

                return await contents.ToListAsync();
            }

            return null;
        }

        public async Task<bool> InsertContent(Content content)
        {
            if (_dbContext != null)
            {
                _dbContext.Contents.Add(content);
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateContent(Content content)
        {
            if (_dbContext != null)
            {
                _dbContext.Contents.Update(content);
                return true;
            }

            return false;
        }

        private static IQueryable<Content> FilterContents(PaginationContentRequest request, IQueryable<Content> contents)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.Module != Guid.Empty && request.Module != null)
            {
                contents = contents.Where(x => x.ModuleId == request.Module);
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                contents = contents.Where(x => x.ContentName.Contains(request.Keyword)
                    || x.Type.Contains(request.Keyword));
            }

            if (request.Published)
            {
                contents = contents.Where(x => x.IsPublished == 1);
            }

            contents = contents.Skip(skip).Take(request.Limit);
            return contents;
        }

        private static IQueryable<Content> SortContents(PaginationContentRequest request, IQueryable<Content> contents)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Content"))
                    {
                        contents = contents.OrderByDescending(s => s.ContentName);
                    }
                    else if (request.Sort.Equals("Type"))
                    {
                        contents = contents.OrderByDescending(s => s.Type);
                    }
                    else
                    {
                        contents = contents.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Content"))
                    {
                        contents = contents.OrderBy(s => s.ContentName);
                    }
                    else if (request.Sort.Equals("Type"))
                    {
                        contents = contents.OrderBy(s => s.Type);
                    }
                    else
                    {
                        contents = contents.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return contents;
        }
    }
}
