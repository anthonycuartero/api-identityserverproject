﻿using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Repository.Dapper;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ELearningContext _dbContext;
        private readonly IConfiguration _configuration;

        public UnitOfWork(ELearningContext dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        //public ISubjectRepository SubjectRepository => new SubjectRepositoryEF(_dbContext);
        //public ICourseRepository CourseRepository => new CourseRepositoryEF(_dbContext);
        //public IModulesRepository ModuleRepository => new ModulesRepositoryEF(_dbContext);
        //public IContentRepository ContentRepository => new ContentRepositoryEF(_dbContext);
        //public IEnrollmentsRepository EnrollmentsRepository => new EnrollmentsRepositoryEF(_dbContext);

        public ISubjectRepository SubjectRepository => new SubjectRepository(_configuration);
        public ICourseRepository CourseRepository => new CourseRepository(_configuration);
        public IModulesRepository ModuleRepository => new ModulesRepository(_configuration);
        public IContentRepository ContentRepository => new ContentRepository(_configuration);
        public IEnrollmentsRepository EnrollmentsRepository => new EnrollmentsRepository(_configuration);

        public async Task<bool> SaveAsync()
        {
            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}
