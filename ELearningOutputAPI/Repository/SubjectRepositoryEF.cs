﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository
{
    public class SubjectRepositoryEF : ISubjectRepository
    {
        private ELearningContext _dbContext;

        public SubjectRepositoryEF(ELearningContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> DeleteSubject(Guid subjectId)
        {
            if (_dbContext != null)
            {
                var subject = await GetSpecificSubject(subjectId);
                _dbContext.Subjects.Remove(subject);
                return true;
            }

            return false;
        }

        public async Task<Subject> GetSpecificSubject(Guid subjectId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Subjects.FirstOrDefaultAsync(x => x.Id == subjectId);
            }

            return null;
        }

        public async Task<List<Subject>> GetSubjects(PaginationSubjectRequest request)
        {
            if (_dbContext != null)
            {
                if (request == null)
                {
                    return await _dbContext.Subjects.ToListAsync();
                }

                var subjects = _dbContext.Subjects.AsQueryable();

                subjects = FilterSubjects(request, subjects);

                subjects = SortSubjects(request, subjects);

                return await subjects.ToListAsync();
            }

            return null;
        }

        public async Task<bool> InsertSubject(Subject subject)
        {
            if (_dbContext != null)
            {
                _dbContext.Subjects.Add(subject);
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateSubject(Subject subject)
        {
            if (_dbContext != null)
            {
                _dbContext.Subjects.Update(subject);
                return true;
            }

            return false;
        }

        private static IQueryable<Subject> FilterSubjects(PaginationSubjectRequest request, IQueryable<Subject> subjects)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                subjects = subjects.Where(x => x.Title.Contains(request.Keyword));
            }

            if (request.Published)
            {
                subjects = subjects.Where(x => x.IsPublished == 1);
            }

            if (request.Courses)
            {
                subjects = subjects.Include(x => x.Courses);
            }

            subjects = subjects.Skip(skip).Take(request.Limit);
            return subjects;
        }

        private static IQueryable<Subject> SortSubjects(PaginationSubjectRequest request, IQueryable<Subject> subjects)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {

                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Title"))
                    {
                        subjects = subjects.OrderByDescending(s => s.Title);
                    }
                    else if (request.Sort.Equals("Owner"))
                    {
                        subjects = subjects.OrderByDescending(s => s.Owner);
                    }
                    else
                    {
                        subjects = subjects.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Title"))
                    {
                        subjects = subjects.OrderBy(s => s.Title);
                    }
                    else if (request.Sort.Equals("Owner"))
                    {
                        subjects = subjects.OrderBy(s => s.Owner);
                    }
                    else
                    {
                        subjects = subjects.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return subjects;
        }
    }
}
