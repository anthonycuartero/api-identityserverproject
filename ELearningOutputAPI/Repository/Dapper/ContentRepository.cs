﻿using Dapper;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Dapper
{
    public class ContentRepository : IContentRepository
    {
        public string _connectionString { get; set; }
        private SqlConnection _sqlConnection;
        private readonly IConfiguration _configuration;

        public ContentRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            var connection = _configuration.GetConnectionString("IdentityConnection");
            _connectionString = connection.Insert(connection.LastIndexOf(';') + 1, "Initial Catalog=ELearningOutputDB3;");
        }


        public async Task<bool> DeleteContent(Guid contentId)
        {
            string sqlQuery = "DELETE FROM Content where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, new { Id = contentId});

                return true;
            }
        }

        public async Task<Content> GetSpecificContent(Guid contentId)
        {
            string sqlQuery = "SELECT TOP 1 * from Content where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = await _sqlConnection.QueryFirstOrDefaultAsync<Content>(sqlQuery, new { Id = contentId});

                if (result != null)
                    return result;
            }

            return null;
        }

        public async Task<List<Content>> GetContents(PaginationContentRequest request)
        {
            string sqlQuery = "SELECT * from Content";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var contents = _sqlConnection.Query<Content>(sqlQuery).AsQueryable();

                contents = FilterContents(request, contents);

                contents = SortContents(request, contents);

                return contents.ToList();
            }
        }

        public async Task<bool> InsertContent(Content content)
        {
            string sqlQuery = "INSERT into Content " +
                    "(Id, ModuleId, ContentName, Type, IsPublished, CreatedAt, UpdatedAt) " +
                    "VALUES (@Id, @ModuleId, @ContentName, @Type, @IsPublished, @CreatedAt, @UpdatedAt)";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, content);

                return true;
            }
        }

        public async Task<bool> UpdateContent(Content content)
        {
            string sqlQuery = "UPDATE Content SET " +
                    "ContentName = @ContentName, UpdatedAt = @UpdatedAt, IsPublished = @IsPublished, Type = @Type " +
                    "WHERE Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, content);

                return true;
            }
        }

        private static IQueryable<Content> FilterContents(PaginationContentRequest request, IQueryable<Content> contents)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.Module != Guid.Empty && request.Module != null)
            {
                contents = contents.Where(x => x.ModuleId == request.Module);
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                contents = contents.Where(x => x.ContentName.Contains(request.Keyword)
                    || x.Type.Contains(request.Keyword));
            }

            if (request.Published)
            {
                contents = contents.Where(x => x.IsPublished == 1);
            }

            contents = contents.Skip(skip).Take(request.Limit);
            return contents;
        }

        private static IQueryable<Content> SortContents(PaginationContentRequest request, IQueryable<Content> contents)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Content"))
                    {
                        contents = contents.OrderByDescending(s => s.ContentName);
                    }
                    else if (request.Sort.Equals("Type"))
                    {
                        contents = contents.OrderByDescending(s => s.Type);
                    }
                    else
                    {
                        contents = contents.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Content"))
                    {
                        contents = contents.OrderBy(s => s.ContentName);
                    }
                    else if (request.Sort.Equals("Type"))
                    {
                        contents = contents.OrderBy(s => s.Type);
                    }
                    else
                    {
                        contents = contents.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return contents;
        }

    }
}
