﻿using Dapper;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Dapper
{
    public class SubjectRepository : ISubjectRepository
    {
        public string _connectionString { get; set; }
        private SqlConnection _sqlConnection;
        private readonly IConfiguration _configuration;

        public SubjectRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            var connection = _configuration.GetConnectionString("IdentityConnection");
            _connectionString = connection.Insert(connection.LastIndexOf(';') + 1, "Initial Catalog=ELearningOutputDB3;");
        }

        public async Task<bool> DeleteSubject(Guid subjectId)
        {
            string sqlQuery = "DELETE FROM Subject where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, new { Id = subjectId });

                return true;
            }
        }

        public async Task<Subject> GetSpecificSubject(Guid subjectId)
        {
            string sqlQuery = "SELECT TOP 1 * from Subject where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = await _sqlConnection.QueryFirstOrDefaultAsync<Subject>(sqlQuery, new { Id = subjectId });

                if (result != null)
                    return result;
            }

            return null;
        }

        public async Task<List<Subject>> GetSubjects(PaginationSubjectRequest request)
        {
            string sqlQuery = "SELECT * from Subject";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var contents = _sqlConnection.Query<Subject>(sqlQuery).AsQueryable();

                contents = FilterSubjects(request, contents);

                contents = SortSubjects(request, contents);

                return contents.ToList();
            }
        }

        public async Task<bool> InsertSubject(Subject subject)
        {
            string sqlQuery = "INSERT into Subject " +
                     "(Id, Title, IsPublished, Owner, CreatedAt, UpdatedAt) " +
                     "VALUES (@Id, @Title, @IsPublished, @Owner, @CreatedAt, @UpdatedAt)";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, subject);

                return true;
            }
        }

        public async Task<bool> UpdateSubject(Subject subject)
        {
            string sqlQuery = "UPDATE Subject SET " +
                    "Title = @Title, Owner = @Owner, IsPublished = @IsPublished, UpdatedAt = @UpdatedAt " +
                    "WHERE Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, subject);

                return true;
            }
        }

        private static IQueryable<Subject> FilterSubjects(PaginationSubjectRequest request, IQueryable<Subject> subjects)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                subjects = subjects.Where(x => x.Title.Contains(request.Keyword));
            }

            if (request.Published)
            {
                subjects = subjects.Where(x => x.IsPublished == 1);
            }

            subjects = subjects.Skip(skip).Take(request.Limit);
            return subjects;
        }

        private static IQueryable<Subject> SortSubjects(PaginationSubjectRequest request, IQueryable<Subject> subjects)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {

                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Title"))
                    {
                        subjects = subjects.OrderByDescending(s => s.Title);
                    }
                    else if (request.Sort.Equals("Owner"))
                    {
                        subjects = subjects.OrderByDescending(s => s.Owner);
                    }
                    else
                    {
                        subjects = subjects.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Title"))
                    {
                        subjects = subjects.OrderBy(s => s.Title);
                    }
                    else if (request.Sort.Equals("Owner"))
                    {
                        subjects = subjects.OrderBy(s => s.Owner);
                    }
                    else
                    {
                        subjects = subjects.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return subjects;
        }
    }
}
