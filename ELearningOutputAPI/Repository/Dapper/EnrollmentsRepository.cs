﻿using Dapper;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Dapper
{
    public class EnrollmentsRepository : IEnrollmentsRepository
    {
        public string _connectionString { get; set; }
        private SqlConnection _sqlConnection;
        private readonly IConfiguration _configuration;

        public EnrollmentsRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            var connection = _configuration.GetConnectionString("IdentityConnection");
            _connectionString = connection.Insert(connection.LastIndexOf(';') + 1, "Initial Catalog=ELearningOutputDB3;");
        }

        public async Task<bool> DeleteEnrollment(Guid enrollmentId)
        {
            string sqlQuery = "DELETE FROM Enrollment where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, new { Id = enrollmentId });

                return true;
            }
        }

        public async Task<Enrollment> GetSpecificEnrollment(Guid enrollmentId)
        {
            string sqlQuery = "SELECT TOP 1 * from Enrollment where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = await _sqlConnection.QueryFirstOrDefaultAsync<Enrollment>(sqlQuery, new { Id = enrollmentId });

                if (result != null)
                    return result;
            }

            return null;
        }

        public async Task<Enrollment> GetExistingEnrollment(Guid courseId, Guid studentId)
        {
            string sqlQuery = "SELECT TOP 1 * from Enrollment where CourseId = @CourseId and UserId = @StudentId";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = await _sqlConnection.QueryFirstOrDefaultAsync<Enrollment>(sqlQuery, new { CourseId = courseId, StudentId = studentId });

                if (result != null)
                    return result;
            }

            return null;
        }

        public async Task<List<Enrollment>> GetEnrollments(PaginationEnrollmentRequest request)
        {
            string sqlQuery = "SELECT * from Enrollment";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var enrollments = _sqlConnection.Query<Enrollment>(sqlQuery).AsQueryable();

                enrollments = FilterEnrollments(request, enrollments);

                enrollments = SortEnrollments(request, enrollments);

                return enrollments.ToList();
            }
        }

        public async Task<bool> InsertEnrollment(Enrollment enrollment)
        {
            string sqlQuery = "INSERT into Enrollment " +
                     "(Id, UserId, CourseId, CreatedAt, UpdatedAt, IsCompleted) " +
                     "VALUES (@Id, @UserId, @CourseId, @CreatedAt, @UpdatedAt, @IsCompleted)";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, enrollment);

                return true;
            }
        }

        public async Task<bool> UpdateEnrollment(Enrollment enrollment)
        {
            string sqlQuery = "UPDATE Enrollment SET " +
                    "IsCompleted = @IsCompleted, UpdatedAt = @UpdatedAt " +
                    "WHERE Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, enrollment);

                return true;
            }
        }

        public async Task<bool> InsertCompletedModules(CompletedModules enrollment)
        {
            string sqlQuery = "INSERT into CompletedModule " +
                     "(Id, ModuleId, UserId, CourseId, CreatedAt) " +
                     "VALUES (@Id, @ModuleId, @UserId, @CourseId, @CreatedAt)";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, enrollment);

                return true;
            }
        }

        public async Task<List<CompletedModules>> GetCompletedModulesByUserAndCourse(Guid userId, Guid courseId)
        {

            string sqlQuery = "SELECT * from CompletedModule where CourseId = @CourseId and UserId = @UserId";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = await _sqlConnection.QueryAsync<CompletedModules>(sqlQuery, new { CourseId = courseId, UserId = userId });

                if (result != null)
                    return result.ToList();
            }

            return null;
        }

        private static IQueryable<Enrollment> FilterEnrollments(PaginationEnrollmentRequest request, IQueryable<Enrollment> enrollments)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.Student != Guid.Empty && request.Student != null)
            {
                enrollments = enrollments.Where(x => x.UserId == request.Student);
            }

            if (request.Completed)
            {
                enrollments = enrollments.Where(x => x.IsCompleted == 1);
            }

            enrollments = enrollments.Skip(skip).Take(request.Limit);
            return enrollments;
        }

        private static IQueryable<Enrollment> SortEnrollments(PaginationEnrollmentRequest request, IQueryable<Enrollment> modules)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    modules = modules.OrderByDescending(s => s.CreatedAt);
                }
                else
                {
                    modules = modules.OrderBy(s => s.CreatedAt);
                }
            }

            return modules;
        }
    }
}
