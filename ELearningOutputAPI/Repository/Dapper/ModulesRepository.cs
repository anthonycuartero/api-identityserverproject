﻿
using Dapper;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Dapper
{
    public class ModulesRepository : IModulesRepository
    {
        public string _connectionString { get; set; }
        private SqlConnection _sqlConnection;
        private readonly IConfiguration _configuration;

        public ModulesRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            var connection = _configuration.GetConnectionString("IdentityConnection");
            _connectionString = connection.Insert(connection.LastIndexOf(';') + 1, "Initial Catalog=ELearningOutputDB3;");
        }

        public async Task<bool> DeleteModule(Guid moduleId)
        {
            string sqlQuery = "DELETE FROM Module where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, new { Id = moduleId });

                return true;
            }
        }

        public async Task<Module> GetSpecificModule(Guid moduleId)
        {
            string sqlQuery = "SELECT TOP 1 * from Module where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = await _sqlConnection.QueryFirstOrDefaultAsync<Module>(sqlQuery, new { Id = moduleId });

                if (result != null)
                    return result;
            }

            return null;
        }

        public async Task<List<Module>> GetModules(PaginationModuleRequest request)
        {
            string sqlQuery = "SELECT * from Module";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var contents = _sqlConnection.Query<Module>(sqlQuery).AsQueryable();

                contents = FilterModules(request, contents);

                contents = SortModules(request, contents);

                return contents.ToList();
            }
        }

        public async Task<bool> InsertModule(Module module)
        {
            string sqlQuery = "INSERT into Module " +
                     "(Id, Title, Duration, CourseId, IsPublished, CourseTitle, CreatedAt, UpdatedAt, Version) " +
                     "VALUES (@Id, @Title, @Duration, @CourseId, @IsPublished, @CourseTitle, @CreatedAt, @UpdatedAt, @Version)";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, module);

                return true;
            }
        }

        public async Task<bool> UpdateModule(Module module)
        {
            string sqlQuery = "UPDATE Module SET " +
                    "Title = @Title, Duration = @Duration, IsPublished = @IsPublished, CourseTitle = @CourseTitle, Version = @Version, " +
                    "UpdatedAt = @UpdatedAt " +
                    "WHERE Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, module);

                return true;
            }
        }

        public async Task<List<Module>> GetModulesByCourseId(Guid courseId)
        {
            string sqlQuery = "SELECT * from Module where CourseId = @CourseId";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = _sqlConnection.Query<Module>(sqlQuery, new { Id = courseId });

                if (result != null)
                    return result.ToList();
            }

            return null;
        }

        private static IQueryable<Module> FilterModules(PaginationModuleRequest request, IQueryable<Module> modules)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.Course != Guid.Empty && request.Course != null)
            {
                modules = modules.Where(x => x.CourseId == request.Course);
            }

            if (request.Duration > 0)
            {
                modules = modules.Where(x => x.Duration == request.Duration);
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                modules = modules.Where(x => x.Title.Contains(request.Keyword)
                    || x.CourseTitle.Contains(request.Keyword));
            }

            if (request.Published)
            {
                modules = modules.Where(x => x.IsPublished == 1);
            }

            modules = modules.Skip(skip).Take(request.Limit);
            return modules;
        }

        private static IQueryable<Module> SortModules(PaginationModuleRequest request, IQueryable<Module> modules)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Title"))
                    {
                        modules = modules.OrderByDescending(s => s.Title);
                    }
                    else if (request.Sort.Equals("CourseTitle"))
                    {
                        modules = modules.OrderByDescending(s => s.CourseTitle);
                    }
                    else
                    {
                        modules = modules.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Title"))
                    {
                        modules = modules.OrderBy(s => s.Title);
                    }
                    else if (request.Sort.Equals("CourseTitle"))
                    {
                        modules = modules.OrderBy(s => s.CourseTitle);
                    }
                    else
                    {
                        modules = modules.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return modules;
        }
    }
}
