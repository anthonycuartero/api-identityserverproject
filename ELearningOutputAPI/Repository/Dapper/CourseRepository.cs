﻿using Dapper;
using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository.Dapper
{
    public class CourseRepository : ICourseRepository
    {
        public string _connectionString { get; set; }
        private SqlConnection _sqlConnection;
        private readonly IConfiguration _configuration;

        public CourseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            var connection = _configuration.GetConnectionString("IdentityConnection");
            _connectionString = connection.Insert(connection.LastIndexOf(';') + 1, "Initial Catalog=ELearningOutputDB3;");
        }

        public async Task<bool> DeleteCourse(Guid courseId)
        {
            string sqlQuery = "DELETE FROM Course where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, new { Id = courseId });

                return true;
            }
        }

        public async Task<Course> GetSpecificCourse(Guid courseId)
        {
            string sqlQuery = "SELECT TOP 1 * from Course where Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = await _sqlConnection.QueryFirstOrDefaultAsync<Course>(sqlQuery, new { Id = courseId });

                if (result != null)
                    return result;
            }

            return null;
        }

        public async Task<List<Course>> GetCoursesByIds(List<Guid> courseIds)
        {
            string sqlQuery = "SELECT * from Course";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var result = _sqlConnection.Query<Course>(sqlQuery).AsQueryable().Where(x => courseIds.Contains(x.Id));

                if (result != null)
                    return result.ToList();
            }

            return null;
        }

        public async Task<List<Course>> GetCourses(PaginationCourseRequest request)
        {
            string sqlQuery = "SELECT * from Course";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                var contents = _sqlConnection.Query<Course>(sqlQuery).AsQueryable();

                contents = FilterCourses(request, contents);

                contents = SortCourses(request, contents);

                return contents.ToList();
            }
        }

        public async Task<bool> InsertCourse(Course course)
        {
            string sqlQuery = "INSERT into Course " +
                     "(Id, SubjectId, Title, Description, Icon, Author, Duration, IsPublished, CreatedAt, UpdatedAt) " +
                     "VALUES (@Id, @SubjectId, @Title, @Description, @Icon, @Author, @Duration, @IsPublished, @CreatedAt, @UpdatedAt)";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, course);

                return true;
            }
        }

        public async Task<bool> UpdateCourse(Course course)
        {
            string sqlQuery = "UPDATE Course SET " +
                    "Title = @Title, Description = @Description, Author = @Author, Duration = @Duration, IsPublished = @IsPublished, " +
                    "UpdatedAt = @UpdatedAt " +
                    "WHERE Id = @Id";

            using (_sqlConnection = new SqlConnection(_connectionString))
            {
                await _sqlConnection.ExecuteAsync(sqlQuery, course);

                return true;
            }
        }

        private static IQueryable<Course> FilterCourses(PaginationCourseRequest request, IQueryable<Course> course)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.SubjectId != Guid.Empty && request.SubjectId != null)
            {
                course = course.Where(x => x.SubjectId == request.SubjectId);
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                course = course.Where(x => x.Title.Contains(request.Keyword)
                    || x.Description.Contains(request.Keyword)
                    || x.Author.Contains(request.Keyword));
            }

            if (request.Published)
            {
                course = course.Where(x => x.IsPublished == 1);
            }

            course = course.Skip(skip).Take(request.Limit);
            return course;
        }

        private static IQueryable<Course> SortCourses(PaginationCourseRequest request, IQueryable<Course> course)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Title"))
                    {
                        course = course.OrderByDescending(s => s.Title);
                    }
                    else if (request.Sort.Equals("Description"))
                    {
                        course = course.OrderByDescending(s => s.Description);
                    }
                    else if (request.Sort.Equals("Author"))
                    {
                        course = course.OrderByDescending(s => s.Author);
                    }
                    else
                    {
                        course = course.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Title"))
                    {
                        course = course.OrderBy(s => s.Title);
                    }
                    else if (request.Sort.Equals("Description"))
                    {
                        course = course.OrderBy(s => s.Description);
                    }
                    else if (request.Sort.Equals("Author"))
                    {
                        course = course.OrderBy(s => s.Author);
                    }
                    else
                    {
                        course = course.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return course;
        }
    }
}
