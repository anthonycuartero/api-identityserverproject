﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository
{
    public class ModulesRepositoryEF : IModulesRepository
    {
        private ELearningContext _dbContext;

        public ModulesRepositoryEF(ELearningContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> DeleteModule(Guid moduleId)
        {
            if (_dbContext != null)
            {
                var course = await GetSpecificModule(moduleId);
                _dbContext.Modules.Remove(course);
                return true;
            }

            return false;
        }

        public async Task<Module> GetSpecificModule(Guid moduleId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Modules.FirstOrDefaultAsync(x => x.Id == moduleId);
            }

            return null;
        }

        public async Task<List<Module>> GetModules(PaginationModuleRequest request)
        {
            if (_dbContext != null)
            {
                if (request == null)
                {
                    return await _dbContext.Modules.ToListAsync();
                }

                var modules = _dbContext.Modules.AsQueryable();

                modules = FilterModules(request, modules);

                modules = SortModules(request, modules);

                return await modules.ToListAsync();
            }

            return null;
        }

        public async Task<bool> InsertModule(Module module)
        {
            if (_dbContext != null)
            {
                _dbContext.Modules.Add(module);
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateModule(Module module)
        {
            if (_dbContext != null)
            {
                _dbContext.Modules.Update(module);
                return true;
            }

            return false;
        }

        public async Task<List<Module>> GetModulesByCourseId(Guid courseId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Modules.Where(x => x.CourseId == courseId).ToListAsync();
            }

            return null;
        }

        private static IQueryable<Module> FilterModules(PaginationModuleRequest request, IQueryable<Module> modules)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.Course != Guid.Empty && request.Course != null)
            {
                modules = modules.Where(x => x.CourseId == request.Course);
            }

            if (request.Duration > 0)
            {
                modules = modules.Where(x => x.Duration == request.Duration);
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                modules = modules.Where(x => x.Title.Contains(request.Keyword)
                    || x.CourseTitle.Contains(request.Keyword));
            }

            if (request.Published)
            {
                modules = modules.Where(x => x.IsPublished == 1);
            }

            if (request.HasContents)
            {
                modules = modules.Include(x => x.Contents);
            }

            modules = modules.Skip(skip).Take(request.Limit);
            return modules;
        }

        private static IQueryable<Module> SortModules(PaginationModuleRequest request, IQueryable<Module> modules)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Title"))
                    {
                        modules = modules.OrderByDescending(s => s.Title);
                    }
                    else if (request.Sort.Equals("CourseTitle"))
                    {
                        modules = modules.OrderByDescending(s => s.CourseTitle);
                    }
                    else
                    {
                        modules = modules.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Title"))
                    {
                        modules = modules.OrderBy(s => s.Title);
                    }
                    else if (request.Sort.Equals("CourseTitle"))
                    {
                        modules = modules.OrderBy(s => s.CourseTitle);
                    }
                    else
                    {
                        modules = modules.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return modules;
        }
    }
}
