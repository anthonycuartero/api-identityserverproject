﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository
{
    public class CourseRepositoryEF : ICourseRepository
    {
        private ELearningContext _dbContext;

        public CourseRepositoryEF(ELearningContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> DeleteCourse(Guid courseId)
        {
            if (_dbContext != null)
            {
                var course = await GetSpecificCourse(courseId);
                _dbContext.Courses.Remove(course);
                return true;
            }

            return false;
        }

        public async Task<Course> GetSpecificCourse(Guid courseId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Courses.FirstOrDefaultAsync(x => x.Id == courseId);
            }

            return null;
        }

        public async Task<List<Course>> GetCoursesByIds(List<Guid> courseIds)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Courses.Where(x => courseIds.Contains(x.Id)).ToListAsync();
            }

            return null;
        }

        public async Task<List<Course>> GetCourses(PaginationCourseRequest request)
        {
            if (_dbContext != null)
            {
                if (request == null)
                {
                    return await _dbContext.Courses.ToListAsync();
                }

                var courses = _dbContext.Courses.AsQueryable();

                courses = FilterCourses(request, courses);

                courses = SortCourses(request, courses);

                return await courses.ToListAsync();
            }

            return null;
        }

        public async Task<bool> InsertCourse(Course course)
        {
            if (_dbContext != null)
            {
                _dbContext.Courses.Add(course);
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateCourse(Course course)
        {
            if (_dbContext != null)
            {
                _dbContext.Courses.Update(course);
                return true;
            }

            return false;
        }

        private static IQueryable<Course> FilterCourses(PaginationCourseRequest request, IQueryable<Course> course)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.SubjectId != Guid.Empty && request.SubjectId != null)
            {
                course = course.Where(x => x.SubjectId == request.SubjectId);
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                course = course.Where(x => x.Title.Contains(request.Keyword) 
                    || x.Description.Contains(request.Keyword) 
                    || x.Author.Contains(request.Keyword));
            }

            if (request.Published)
            {
                course = course.Where(x => x.IsPublished == 1);
            }

            if (request.Sections)
            {
                course = course.Include(x => x.Modules);
            }

            course = course.Skip(skip).Take(request.Limit);
            return course;
        }

        private static IQueryable<Course> SortCourses(PaginationCourseRequest request, IQueryable<Course> course)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    if (request.Sort.Equals("Title"))
                    {
                        course = course.OrderByDescending(s => s.Title);
                    }
                    else if (request.Sort.Equals("Description"))
                    {
                        course = course.OrderByDescending(s => s.Description);
                    }
                    else if (request.Sort.Equals("Author"))
                    {
                        course = course.OrderByDescending(s => s.Author);
                    }
                    else
                    {
                        course = course.OrderByDescending(s => s.CreatedAt);
                    }
                }
                else
                {
                    if (request.Sort.Equals("Title"))
                    {
                        course = course.OrderBy(s => s.Title);
                    }
                    else if (request.Sort.Equals("Description"))
                    {
                        course = course.OrderBy(s => s.Description);
                    }
                    else if (request.Sort.Equals("Author"))
                    {
                        course = course.OrderBy(s => s.Author);
                    }
                    else
                    {
                        course = course.OrderBy(s => s.CreatedAt);
                    }
                }
            }

            return course;
        }
    }
}
