﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Repository
{
    public class EnrollmentsRepositoryEF : IEnrollmentsRepository
    {
        private ELearningContext _dbContext;

        public EnrollmentsRepositoryEF(ELearningContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> DeleteEnrollment(Guid enrollmentId)
        {
            if (_dbContext != null)
            {
                var enrollment = await GetSpecificEnrollment(enrollmentId);
                _dbContext.Enrollments.Remove(enrollment);
                return true;
            }

            return false;
        }

        public async Task<Enrollment> GetSpecificEnrollment(Guid enrollmentId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Enrollments.FirstOrDefaultAsync(x => x.Id == enrollmentId);
            }

            return null;
        }

        public async Task<Enrollment> GetExistingEnrollment(Guid courseId, Guid studentId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.Enrollments.FirstOrDefaultAsync(x => x.CourseId == courseId && x.UserId == studentId);
            }

            return null;
        }

        public async Task<List<Enrollment>> GetEnrollments(PaginationEnrollmentRequest request)
        {
            if (_dbContext != null)
            {
                if (request == null)
                {
                    return await _dbContext.Enrollments.ToListAsync();
                }

                var enrollments = _dbContext.Enrollments.AsQueryable();

                enrollments = FilterEnrollments(request, enrollments);

                enrollments = SortEnrollments(request, enrollments);

                return await enrollments.ToListAsync();
            }

            return null;
        }

        public async Task<bool> InsertEnrollment(Enrollment enrollment)
        {
            if (_dbContext != null)
            {
                _dbContext.Enrollments.Add(enrollment);
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateEnrollment(Enrollment enrollment)
        {
            if (_dbContext != null)
            {
                _dbContext.Enrollments.Update(enrollment);
                return true;
            }

            return false;
        }

        public async Task<bool> InsertCompletedModules(CompletedModules enrollment)
        {
            if (_dbContext != null)
            {
                _dbContext.CompletedModules.Add(enrollment);
                return true;
            }

            return false;
        }

        public async Task<List<CompletedModules>> GetCompletedModulesByUserAndCourse(Guid userId, Guid courseId)
        {
            if (_dbContext != null)
            {
                return await _dbContext.CompletedModules.Where(x => x.UserId == userId && x.CourseId == courseId).ToListAsync();
            }

            return null;
        }

        private static IQueryable<Enrollment> FilterEnrollments(PaginationEnrollmentRequest request, IQueryable<Enrollment> enrollments)
        {
            var skip = (request.Page - 1) * request.Limit;

            if (request.Student != Guid.Empty && request.Student != null)
            {
                enrollments = enrollments.Where(x => x.UserId == request.Student);
            }

            if (request.Completed)
            {
                enrollments = enrollments.Where(x => x.IsCompleted == 1);
            }

            enrollments = enrollments.Skip(skip).Take(request.Limit);
            return enrollments;
        }

        private static IQueryable<Enrollment> SortEnrollments(PaginationEnrollmentRequest request, IQueryable<Enrollment> modules)
        {
            if (!string.IsNullOrEmpty(request.Sort))
            {
                if (request.SortDirection.Equals("DESC"))
                {
                    modules = modules.OrderByDescending(s => s.CreatedAt);
                }
                else
                {
                    modules = modules.OrderBy(s => s.CreatedAt);
                }
            }

            return modules;
        }
    }
}
