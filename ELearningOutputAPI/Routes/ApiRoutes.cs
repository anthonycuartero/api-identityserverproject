﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Routes
{
    public static class ApiRoutes
    {
        public static class Accounts
        {
            public const string Login = "login";

            public const string Signup = "signup";

            public const string Logout = "logout";

            public const string ResetPassword = "password";

            public const string RequestResetPassword = "password";

            public const string ResendVerificationEmail = "signup/verification";

            public const string VerifyUser = "signup/verification";

            public const string ExternalLogin = "externallogin";
        }

        public static class Users
        {
            public const string GetUsers = "users";

            public const string GetUserDetails = "users/{id}";
        }

        public static class Subjects
        {
            public const string InsertSubjects = "subjects";

            public const string GetSubjects = "subjects";

            public const string GetSpecificSubject = "subjects/{id}";

            public const string UpdateSubject = "subjects/{id}";

            public const string DeleteSubject = "subjects/{id}";
        }

        public static class Courses
        {
            public const string InsertCourses = "courses";

            public const string GetCourses = "courses";

            public const string GetSpecificCourse = "courses/{id}";

            public const string UpdateCourse = "courses/{id}";

            public const string DeleteCourse = "courses/{id}";
        }

        public static class Modules
        {
            public const string InsertModule = "modules";

            public const string GetModules = "modules";

            public const string GetSpecificModule = "modules/{id}";

            public const string UpdateModule = "modules/{id}";

            public const string DeleteModule = "modules/{id}";
        }

        public static class Contents
        {
            public const string InsertContent = "contents";

            public const string GetContents = "contents";

            public const string GetSpecificContent = "contents/{id}";

            public const string UpdateContent = "contents/{id}";

            public const string DeleteContent = "contents/{id}";
        }

        public static class Enrollments
        {
            public const string InsertEnrollment = "enrollments";

            public const string GetEnrollments = "enrollments";

            public const string GetSpecificEnrollment = "enrollments/{id}";

            public const string UpdateEnrollment = "enrollments/{id}";

            public const string DeleteEnrollment = "enrollments/{id}";
        }

        public static class Values
        {
            public const string Value = "values";
        }
    }
}
