﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using ELearningOutputAPI.Service.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Service
{
    public class AccountsService : IAccountsService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public ApplicationSettings _appSettings;
        private static readonly HttpClient _httpClient = new HttpClient();

        public AccountsService(UserManager<ApplicationUser> userManager,
            IOptions<ApplicationSettings> appSettings)
        {
            _userManager = userManager;
            _appSettings = appSettings.Value;
        }

        public async Task<ApplicationUser> GetUserDetailsByEmail(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<HttpResponseMessage> ExternalLogin(ExternalLoginModel loginModel)
        {
            var user = new ApplicationUser();
            user = await _userManager.FindByEmailAsync(loginModel.Email);

            if (user == null)
            {
                var appUserModel = new ApplicationUserResponse
                {
                    Id = loginModel.Id,
                    Email = loginModel.Email,
                    FirstName = loginModel.FirstName,
                    LastName = loginModel.LastName,
                    Role = "Instructor",
                    Password = "DefaultExternalPassword123",
                };

                var authResponse = await SignupAsync(appUserModel);

                user = new ApplicationUser
                {
                    Email = appUserModel.Email,
                    PasswordHash = appUserModel.Password,
                };
            }

            var values = new Dictionary<string, string>
            {
                { "grant_type", "password" },
                { "client_id", "postman" },
                { "client_secret", "secret" },
                { "scope", "apiscope" },
                { "username", user.Email },
                { "password", "DefaultExternalPassword123" },
            };

            var content = new FormUrlEncodedContent(values);

            var response = await _httpClient.PostAsync("https://localhost:44359/connect/token", content);
            return response;
        }

        public async Task<DataResponse> SignupAsync(ApplicationUserResponse userModel)
        {
            var existingUser = await _userManager.FindByEmailAsync(userModel.Email);

            if (existingUser != null)
            {
                return new DataResponse
                {
                    Error = new[] { "User email already existed" }
                };
            }

            var applicationUser = new ApplicationUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = userModel.Email,
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                Email = userModel.Email,
            };

            var result = await _userManager.CreateAsync(applicationUser, userModel.Password);
            if (!result.Succeeded)
            {
                return new DataResponse
                {
                    Error = result.Errors.Select(x => x.Description)
                };
            }

            await _userManager.AddToRoleAsync(applicationUser, userModel.Role);
            var user = await _userManager.FindByEmailAsync(userModel.Email);
            return new DataResponse
            {
                Data = new
                {
                    user.Id,
                    user.Email,
                    userModel.Role,
                    user.FirstName,
                    user.LastName,
                },
                Success = true,
            };
        }

        public async Task<DataResponse> LoginAsync(LoginRequest loginModel)
        {
            //var user = await _userManager.FindByNameAsync(loginModel.Username);

            //if (await IsUserExist(loginModel, user))
            //{
            //    var role = await _userManager.GetRolesAsync(user);
            //    var secretToken = _appSettings.JWT_Secrete;
            //    var key = Encoding.ASCII.GetBytes(secretToken);

            //    SecurityTokenDescriptor tokenDescriptor = SetTokenDescriptor(user, key, role.FirstOrDefault());
            //    var tokenHandler = new JwtSecurityTokenHandler();
            //    var security = tokenHandler.CreateToken(tokenDescriptor);
            //    var token = tokenHandler.WriteToken(security);

            //    return new DataResponse
            //    {
            //        Data = new
            //        {
            //            accessToken = token,
            //            user.Id,
            //            user.Email,
            //            role = role.FirstOrDefault(),
            //            user.FirstName,
            //            user.LastName,
            //        },
            //        Success = true,
            //    };
            //}

            return new DataResponse { Error = new[] { "Invalid Credentials" } };
        }

        public async Task<IdentityResult> ResetPassword(ApplicationUser user, string password)
        {
            var newPassword = _userManager.PasswordHasher.HashPassword(user, password);
            user.PasswordHash = newPassword;
            return await _userManager.UpdateAsync(user);
        }

        //private static SecurityTokenDescriptor SetTokenDescriptor(ApplicationUser user, byte[] key, string role)
        //{
        //    var identityOptions = new IdentityOptions();

        //    return new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[]
        //        {
        //                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
        //                new Claim(JwtRegisteredClaimNames.Email, user.Email),
        //                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        //                new Claim("UserID", user.Id.ToString()),
        //                new Claim(identityOptions.ClaimsIdentity.RoleClaimType, role),
        //                new Claim("UserRole", role),
        //        }),
        //        Expires = DateTime.UtcNow.AddMinutes(10),
        //        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        //    };
        //}

        private async Task<bool> IsUserExist(LoginRequest login, ApplicationUser user)
        {
            return user != null && await _userManager.CheckPasswordAsync(user, login.Password);
        }
    }
}
