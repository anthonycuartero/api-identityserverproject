﻿using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Service.Interface;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Service
{
    public class EmailService : IEmailService
    {
        public readonly IConfiguration _configuration;
        public readonly string _username;
        public readonly string _password;
        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
            _username = _configuration["EmailSettings:Username"].ToString();
            _password = _configuration["EmailSettings:Password"].ToString();
        }

        public async Task<bool> SendEmail(EmailParams email)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("", _username));
            message.To.Add(new MailboxAddress("", email.ToEmail));
            message.Subject= email.Subject;
            message.Body = new TextPart("plain")
            {
                Text = email.Body
            };

            using(var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587, false);
                await client.AuthenticateAsync(_username, _password);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
                return true;
            }

        }
    }
}
