﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Models.Response;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Service.Interface
{
    public interface IAccountsService
    {
        Task<ApplicationUser> GetUserDetailsByEmail(string email);

        Task<HttpResponseMessage> ExternalLogin(ExternalLoginModel loginModel);

        Task<DataResponse> SignupAsync(ApplicationUserResponse userModel);

        Task<DataResponse> LoginAsync(LoginRequest loginModel);

        Task<IdentityResult> ResetPassword(ApplicationUser user, string password);
    }
}
