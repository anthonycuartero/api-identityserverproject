﻿using ELearningOutputAPI.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Service.Interface
{
    public interface IEmailService
    {
        Task<bool> SendEmail(EmailParams email);
    }
}
