﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using IdentityServer4.AspNetIdentity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Installers
{
    public class DbInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("IdentityConnection");
            var connectionString = connection.Insert(connection.LastIndexOf(';') + 1, "Initial Catalog=ELearningOutputDB3;");

            services.AddDbContext<ELearningContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ELearningContext>()
                .AddDefaultTokenProviders();

            



            ConfigurePasswordOptions(services);
        }

        private void ConfigurePasswordOptions(IServiceCollection services)
        {
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredLength = 4;
            });
        }
    }
}
