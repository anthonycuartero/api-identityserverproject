﻿using ELearningOutputAPI.Repository;
using ELearningOutputAPI.Repository.Dapper;
using ELearningOutputAPI.Repository.Interface;
using ELearningOutputAPI.Service;
using ELearningOutputAPI.Service.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELearningOutputAPI.Installers
{
    public class RepositoryInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //services.AddScoped<ISubjectRepository, SubjectRepositoryEF>();
            //services.AddScoped<ICourseRepository, CourseRepositoryEF>();
            //services.AddScoped<IModulesRepository, ModulesRepositoryEF>();
            //services.AddScoped<IContentRepository, ContentRepositoryEF>();
            //services.AddScoped<IEnrollmentsRepository, EnrollmentsRepositoryEF>();

            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<IModulesRepository, ModulesRepository>();
            services.AddScoped<IContentRepository, ContentRepository>();
            services.AddScoped<IEnrollmentsRepository, EnrollmentsRepository>();

            services.AddScoped<IAccountsService, AccountsService>();
            services.AddScoped<IEmailService, EmailService>();
        }
    }
}
