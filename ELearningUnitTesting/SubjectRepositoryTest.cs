using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository;
using ELearningOutputAPI.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ELearningUnitTesting
{
    public class SubjectRepositoryTest
    {

        public SubjectRepositoryTest()
        {

        }

        [Fact]
        public async Task GetSubjects_ShouldReturnAllSubject()
        {
            var query = SetMemoryDatabase();

            var result = await query.GetSubjects(new PaginationSubjectRequest
            {
                Courses = true,
                Keyword = "Software Engineering",
                Limit = 10,
                Page = 1,
            });

            Assert.IsAssignableFrom<List<Subject>>(result);
        }

        [Fact]
        public async Task GetSpecificSubject_ShouldReturnSpecificSubject()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.GetSpecificSubject(id);

            Assert.Equal(id, result.Id);
        }

        

        [Fact]
        public async Task DeleteSubject_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.DeleteSubject(id);

            Assert.True(result);
        }

        [Fact]
        public async Task InsertSubject_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var result = await query.InsertSubject(new Subject
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Title = "Test",
                IsPublished = 1,
                Owner = "Test",
            });

            Assert.True(result);
        }

        [Fact]
        public async Task UpdateSubject_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            var subject = new Subject
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Title = "Test",
                IsPublished = 1,
                Owner = "Test",
            };

            await query.InsertSubject(subject);

            subject.Title = "TestUpdate";

            var result = await query.UpdateSubject(subject);

            Assert.True(result);
        }

        private static SubjectRepositoryEF SetMemoryDatabase()
        {
            var options = new DbContextOptionsBuilder<ELearningContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new ELearningContext(options);

            var query = new SubjectRepositoryEF(context);
            return query;
        }

        private static async Task Seed(SubjectRepositoryEF query, Guid id)
        {
            await query.InsertSubject(new Subject
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Title = "Test",
                IsPublished = 1,
                Owner = "Test",
            });
        }
    }
}
