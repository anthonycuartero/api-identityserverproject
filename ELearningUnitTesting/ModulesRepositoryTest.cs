﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ELearningUnitTesting
{
    public class ModulesRepositoryTest
    {
        public ModulesRepositoryTest()
        {

        }

        [Fact]
        public async Task GetModules_ShouldReturnAllSubject()
        {
            var query = SetMemoryDatabase();

            var result = await query.GetModules(new PaginationModuleRequest
            {
                Course = Guid.NewGuid(),
                Duration = 10,
                HasContents = true,
                Keyword = "Software Engineering",
                Limit = 10,
                Page = 1,
            });

            Assert.IsAssignableFrom<List<Module>>(result);
        }

        [Fact]
        public async Task GetSpecificModule_ShouldReturnSpecificSubject()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.GetSpecificModule(id);

            Assert.Equal(id, result.Id);
        }

        [Fact]
        public async Task DeleteModule_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.DeleteModule(id);

            Assert.True(result);
        }

        [Fact]
        public async Task InsertEnrollment_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var result = await query.InsertModule(new Module
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                CourseId = Guid.NewGuid(),
                CourseTitle = "CourseTitle",
                Duration = 10,
                Title = "Title",
                Version = 2,
            });

            Assert.True(result);
        }

        [Fact]
        public async Task UpdateModule_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            var module = new Module
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                CourseId = Guid.NewGuid(),
                CourseTitle = "CourseTitle",
                Duration = 10,
                Title = "Title",
                Version = 2,
            };

            await query.InsertModule(module);

            module.CourseTitle = "CoureTitleUpdated";
            module.Title = "TitleUpdated";

            var result = await query.UpdateModule(module);

            Assert.True(result);
        }

        private static ModulesRepositoryEF SetMemoryDatabase()
        {
            var options = new DbContextOptionsBuilder<ELearningContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new ELearningContext(options);

            var query = new ModulesRepositoryEF(context);

            return query;
        }

        private static async Task Seed(ModulesRepositoryEF query, Guid id)
        {
            await query.InsertModule(new Module
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                CourseId = Guid.NewGuid(),
                CourseTitle = "CourseTitle",
                Duration = 10,
                Title = "Title",
                Version = 2,
            });
        }
    }
}
