﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ELearningUnitTesting
{
    public class CourseRepositoryTest
    {
        public CourseRepositoryTest()
        {

        }

        [Fact]
        public async Task GetCourse_ShouldReturnAllSubject()
        {
            var query = SetMemoryDatabase();

            var result = await query.GetCourses(new PaginationCourseRequest
            {
                SubjectId = Guid.NewGuid(),
                Keyword = "Software Engineering",
                Limit = 10,
                Page = 1,
            });

            Assert.IsAssignableFrom<List<Course>>(result);
        }

        [Fact]
        public async Task GetSpecificCourse_ShouldReturnSpecificSubject()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.GetSpecificCourse(id);

            Assert.Equal(id, result.Id);
        }

        [Fact]
        public async Task DeleteCourse_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.DeleteCourse(id);

            Assert.True(result);
        }

        [Fact]
        public async Task InsertCourse_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var result = await query.InsertCourse(new Course
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Title = "Test",
                IsPublished = 1,
                Author = "Author",
                Description = "Description",
                SubjectId = Guid.NewGuid(),
                Duration = 10,
            });

            Assert.True(result);
        }

        [Fact]
        public async Task UpdateContent_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            var course = new Course
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Title = "Test",
                IsPublished = 1,
                Author = "Author",
                Description = "Description",
                SubjectId = Guid.NewGuid(),
                Duration = 10,
            };

            await query.InsertCourse(course);

            course.Title = "TestUpdate";

            var result = await query.UpdateCourse(course);

            Assert.True(result);
        }

        private static CourseRepositoryEF SetMemoryDatabase()
        {
            var options = new DbContextOptionsBuilder<ELearningContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new ELearningContext(options);

            var query = new CourseRepositoryEF(context);

            return query;
        }

        private static async Task Seed(CourseRepositoryEF query, Guid id)
        {
            await query.InsertCourse(new Course
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Title = "Test",
                IsPublished = 1,
                Author = "Author",
                Description = "Description",
                SubjectId = Guid.NewGuid(),
                Duration = 10,
            });
        }
    }
}
