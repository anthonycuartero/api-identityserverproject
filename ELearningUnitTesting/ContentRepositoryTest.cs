﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ELearningUnitTesting
{
    public class ContentRepositoryTest
    {

        public ContentRepositoryTest()
        {

        }

        [Fact]
        public async Task GetContents_ShouldReturnAllSubject()
        {
            var query = SetMemoryDatabase();

            var result = await query.GetContents(new PaginationContentRequest
            {
                Module = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                Keyword = "Software Engineering",
                Limit = 10,
                Page = 1,
            });

            Assert.IsAssignableFrom<List<Content>>(result);
        }

        [Fact]
        public async Task GetSpecificContent_ShouldReturnSpecificSubject()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.GetSpecificContent(id);

            Assert.Equal(id, result.Id);
        }



        [Fact]
        public async Task DeleteContent_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.DeleteContent(id);

            Assert.True(result);
        }

        [Fact]
        public async Task InsertContent_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var result = await query.InsertContent(new Content
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                ContentName = "Test",
                IsPublished = 1,
                Type = "Type",
                ModuleId = Guid.NewGuid(),
            });

            Assert.True(result);
        }

        [Fact]
        public async Task UpdateContent_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            var content = new Content
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                ContentName = "Test",
                IsPublished = 1,
                Type = "Type",
                ModuleId = Guid.NewGuid(),
            };

            await query.InsertContent(content);

            content.ContentName = "TestUpdate";

            var result = await query.UpdateContent(content);

            Assert.True(result);
        }

        private static ContentRepositoryEF SetMemoryDatabase()
        {
            var options = new DbContextOptionsBuilder<ELearningContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new ELearningContext(options);

            var query = new ContentRepositoryEF(context);

            return query;
        }

        private static async Task Seed(ContentRepositoryEF query, Guid id)
        {
            await query.InsertContent(new Content
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                ContentName = "Test",
                IsPublished = 1,
                ModuleId = Guid.NewGuid(),
                Type = "Type"
            });
        }
    }
}
