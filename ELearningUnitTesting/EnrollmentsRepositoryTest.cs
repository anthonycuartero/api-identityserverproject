﻿using ELearningOutputAPI.Models;
using ELearningOutputAPI.Models.ContextModel;
using ELearningOutputAPI.Models.Request;
using ELearningOutputAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ELearningUnitTesting
{
    public class EnrollmentsRepositoryTest
    {
        public EnrollmentsRepositoryTest()
        {

        }

        [Fact]
        public async Task GetEnrollment_ShouldReturnAllSubject()
        {
            var query = SetMemoryDatabase();

            var result = await query.GetEnrollments(new PaginationEnrollmentRequest
            {
                Student = Guid.NewGuid(),
                Keyword = "Software Engineering",
                Limit = 10,
                Page = 1,
            });

            Assert.IsAssignableFrom<List<Enrollment>>(result);
        }

        [Fact]
        public async Task GetSpecificEnrollment_ShouldReturnSpecificSubject()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.GetSpecificEnrollment(id);

            Assert.Equal(id, result.Id);
        }

        [Fact]
        public async Task DeleteEnrollment_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            await Seed(query, id);

            var result = await query.DeleteEnrollment(id);

            Assert.True(result);
        }

        [Fact]
        public async Task InsertEnrollment_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var result = await query.InsertEnrollment(new Enrollment
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                CourseId = Guid.NewGuid(),
                IsCompleted = 1,
                UserId = Guid.NewGuid(),
            });

            Assert.True(result);
        }

        [Fact]
        public async Task UpdateEnrollment_ShouldReturnTrue()
        {
            var query = SetMemoryDatabase();

            var id = Guid.NewGuid();

            var enrollment = new Enrollment
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                CourseId = Guid.NewGuid(),
                IsCompleted = 1,
                UserId = Guid.NewGuid(),
            };

            await query.InsertEnrollment(enrollment);

            enrollment.IsCompleted = 1;

            var result = await query.UpdateEnrollment(enrollment);

            Assert.True(result);
        }

        private static EnrollmentsRepositoryEF SetMemoryDatabase()
        {
            var options = new DbContextOptionsBuilder<ELearningContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new ELearningContext(options);

            var query = new EnrollmentsRepositoryEF(context);

            return query;
        }

        private static async Task Seed(EnrollmentsRepositoryEF query, Guid id)
        {
            await query.InsertEnrollment(new Enrollment
            {
                Id = id,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                CourseId = Guid.NewGuid(),
                IsCompleted = 1,
                UserId = Guid.NewGuid(),
            });
        }
    }
}
